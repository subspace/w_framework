#include "pack.h"
#include "util.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_FILES_IN_PACK 2048

struct pack_s {
	char *fileName;
	FILE *fileHandle;
	int  numFiles;

	packFile_t *files;
};

//on disk
typedef struct
{
	char	name[56];
	int		filePos, fileLen;
} dpackFile_t;

typedef struct
{
	char	id[4];
	int		dirofs;
	int		dirlen;
} dpackHeader_t;

//on memory

pack_t *packNew(void)
{
	pack_t *pack;

	pack = utilMalloc(sizeof(pack_t));
	memset(pack, 0, sizeof(pack_t));

	return pack;
}

void packDelete(pack_t *pack)
{
	packUnload(pack);

	free(pack);
}

bool packLoad(pack_t *pack, const char *path)
{
	dpackHeader_t	hdr;
	dpackFile_t		*packData;
	int				i;

	pack->fileName = _strdup(path);
	
	pack->fileHandle = fopen(path, "rb");
	if (!pack->fileHandle)
		return false;

	fread(&hdr, 1, sizeof(hdr), pack->fileHandle);
	if (hdr.id[0] != 'P' || hdr.id[1] != 'A' || hdr.id[2] != 'C' || hdr.id[3] != 'K') {
		fclose(pack->fileHandle);
		return false;
	}

	pack->numFiles = hdr.dirlen / sizeof(dpackFile_t);

	if (pack->numFiles > MAX_FILES_IN_PACK) {
		fclose(pack->fileHandle);
		return false;
	}

	pack->files = utilMalloc(sizeof(packFile_t)*pack->numFiles);
	packData = utilMalloc(sizeof(dpackFile_t)*pack->numFiles);

	fseek(pack->fileHandle, hdr.dirofs, SEEK_SET);
	fread(packData, 1, hdr.dirlen, pack->fileHandle);

	for (i = 0; i < pack->numFiles; i++) {
		pack->files[i].fullName = _strdup(packData[i].name);
		pack->files[i].name = _strdup(utilGetFileName(packData[i].name));
		pack->files[i].fileLen = packData[i].fileLen;
		pack->files[i].filePos = packData[i].filePos;
	}

	free(packData);

	return true;
}

bool packUnload(pack_t *pack)
{
	if (pack->fileName)
		free(pack->fileName);

	if (pack->fileHandle)
		fclose(pack->fileHandle);

	if (pack->files) {
		int i;

		for (i = 0; i < pack->numFiles; i++) {
			free(pack->files[i].name);
			free(pack->files[i].fullName);
		}

		free(pack->files);
	}

	memset(pack, 0, sizeof(pack_t));

	return true;
}

packFile_t *packGetFiles(pack_t *pack, int *numFiles)
{
	*numFiles = pack->numFiles;
	if (!*numFiles)
		return NULL;
	return pack->files;
}

byte *packReadFile(pack_t *pack, packFile_t *file)
{
	byte *buf;

	buf = utilMalloc(file->fileLen);
	fseek(pack->fileHandle, file->filePos, SEEK_SET);
	fread(buf, 1, file->fileLen, pack->fileHandle);

	return buf;
}