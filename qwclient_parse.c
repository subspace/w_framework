#include "qwclient.h"
#include "qwclient_local.h"
#include "udp.h"
#include "filesystem.h"
#include "object.h"
#include "timer.h"
#include "rawdata.h"
#include "util.h"
#include "pack.h"
#include "mdfour.h"
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <string.h>

char *svcStrings[] = {
	"svc_bad",
	"svc_nop",
	"svc_disconnect",
	"svc_updatestat",
	"svc_version",		// [long] server version
	"svc_setview",		// [short] entity number
	"svc_sound",		// <see code>
	"svc_time",			// [float] server time
	"svc_print",		// [string] null terminated string
	"svc_stufftext",	// [string] stuffed into client's console buffer
						// the string should be \n terminated
	"svc_setangle",		// [vec3] set the view angle to this absolute value

	"svc_serverdata",	// [long] version ...
	"svc_lightstyle",	// [byte] [string]
	"svc_updatename",	// [byte] [string]
	"svc_updatefrags",	// [byte] [short]
	"svc_clientdata",	// <shortbits + data>
	"svc_stopsound",	// <see code>
	"svc_updatecolors",	// [byte] [byte]
	"svc_particle",		// [vec3] <variable>
	"svc_damage",		// [byte] impact [byte] blood [vec3] from

	"svc_spawnstatic",
	"OBSOLETE svc_spawnbinary",
	"svc_spawnbaseline",

	"svc_temp_entity",	// <variable>
	"svc_setpause",
	"svc_signonnum",
	"svc_centerprint",
	"svc_killedmonster",
	"svc_foundsecret",
	"svc_spawnstaticsound",
	"svc_intermission",
	"svc_finale",

	"svc_cdtrack",
	"svc_sellscreen",

	"svc_smallkick",
	"svc_bigkick",

	"svc_updateping",
	"svc_updateentertime",

	"svc_updatestatlong",
	"svc_muzzleflash",
	"svc_updateuserinfo",
	"svc_download",
	"svc_playerinfo",
	"svc_nails",
	"svc_choke",
	"svc_modellist",
	"svc_soundlist",
	"svc_packetentities",
 	"svc_deltapacketentities",
	"svc_maxspeed",
	"svc_entgravity",

	"svc_setinfo",
	"svc_serverinfo",
	"svc_updatepl",
	"svc_nails2",
	"NEW PROTOCOL",
	"NEW PROTOCOL",
	"NEW PROTOCOL",
	"NEW PROTOCOL",
	"NEW PROTOCOL",
	"NEW PROTOCOL",
	"NEW PROTOCOL",
	"NEW PROTOCOL",
	"NEW PROTOCOL",
	"NEW PROTOCOL",
	"NEW PROTOCOL",
	"NEW PROTOCOL"
};
const int numSvcStrings = sizeof(svcStrings)/sizeof(svcStrings[0]);

static char *qwGetSvcString(int cmd)
{
	if (cmd < numSvcStrings)
		return svcStrings[cmd];
	else
		return "";
}

float rawReadCoord(rawData_t *raw)
{
	return rawReadShort(raw) * (1.0f/8);
}

float rawReadAngle(rawData_t *raw)
{
	return rawReadChar(raw) * (360.0f/256);
}

float rawReadAngle16(rawData_t *raw)
{
	return rawReadShort(raw) * (360.0f/65536);
}
int	bitCounts[32];	/// just for protocol profiling
void qwParseDelta(qwClient_t *qw, entityState_t *from, entityState_t *to, int bits)
{
	int			i;
	int			morebits;

	// set everything to the state we are delta'ing from
	*to = *from;

	to->number = bits & 511;
	bits &= ~511;

	if (bits & U_MOREBITS)
	{	// read in the low order bits
		i = rawReadByte(qw->incomingBuffer);
		bits |= i;
	}

	if (bits & U_FTE_EVENMORE && qw->fteProtocolExtensions) {
		morebits = rawReadByte(qw->incomingBuffer);
		if (morebits & U_FTE_YETMORE)
			morebits |= rawReadByte(qw->incomingBuffer)<<8;
	} else {
		morebits = 0;
	}
	// count the bits for net profiling
	for (i=0 ; i<16 ; i++)
		if (bits&(1<<i))
			bitCounts[i]++;

	to->flags = bits;
	
	if (bits & U_MODEL)
		to->modelindex = rawReadByte(qw->incomingBuffer);
		
	if (bits & U_FRAME)
		to->frame = rawReadByte(qw->incomingBuffer);

	if (bits & U_COLORMAP)
		to->colormap = rawReadByte(qw->incomingBuffer);

	if (bits & U_SKIN)
		to->skinnum = rawReadByte(qw->incomingBuffer);

	if (bits & U_EFFECTS)
		to->effects = rawReadByte(qw->incomingBuffer);

	if (bits & U_ORIGIN1)
		to->origin[0] = rawReadCoord(qw->incomingBuffer);
		
	if (bits & U_ANGLE1)
		to->angles[0] = rawReadAngle(qw->incomingBuffer);

	if (bits & U_ORIGIN2)
		to->origin[1] = rawReadCoord(qw->incomingBuffer);
		
	if (bits & U_ANGLE2)
		to->angles[1] = rawReadAngle(qw->incomingBuffer);

	if (bits & U_ORIGIN3)
		to->origin[2] = rawReadCoord(qw->incomingBuffer);
		
	if (bits & U_ANGLE3)
		to->angles[2] = rawReadAngle(qw->incomingBuffer);

	if (bits & U_SOLID)
	{
		// FIXME
	}

	if (morebits & U_FTE_TRANS && qw->fteProtocolExtensions & FTE_PEXT_TRANS)
		rawReadByte(qw->incomingBuffer);
	if (morebits & U_FTE_ENTITYDBL)
		;
	if (morebits & U_FTE_ENTITYDBL2)
		;
	if (morebits & U_FTE_MODELDBL)
		;
}


void qwFlushEntityPacket(qwClient_t *qw)
{
	int				word;
	entityState_t	olde, newe;

	memset(&olde, 0, sizeof(olde));

	// read it all, but ignore it
	while (1)
	{
		word = (unsigned short)rawReadShort(qw->incomingBuffer);
		
		if (!word)
			break;	// done

		qwParseDelta(qw, &olde, &newe, word);
	}
}

void qwReadDeltaUsercmd(qwClient_t *qw, userCmd_t *from, userCmd_t *move)
{
	int bits;

	memcpy(move, from, sizeof(*move));

	bits = rawReadByte(qw->incomingBuffer);
		
	// read current angles
	if (bits & CM_ANGLE1)
		move->angles[0] = rawReadAngle16(qw->incomingBuffer);
	if (bits & CM_ANGLE2)
		move->angles[1] = rawReadAngle16(qw->incomingBuffer);
	if (bits & CM_ANGLE3)
		move->angles[2] = rawReadAngle16(qw->incomingBuffer);
		
	// read movement
	if (bits & CM_FORWARD)
		move->forwardmove = rawReadShort(qw->incomingBuffer);
	if (bits & CM_SIDE)
		move->sidemove = rawReadShort(qw->incomingBuffer);
	if (bits & CM_UP)
		move->upmove = rawReadShort(qw->incomingBuffer);
	
	// read buttons
	if (bits & CM_BUTTONS)
		move->buttons = rawReadByte(qw->incomingBuffer);

	if (bits & CM_IMPULSE)
		move->impulse = rawReadByte(qw->incomingBuffer);

	// read time to run command
	move->msec = rawReadByte(qw->incomingBuffer);
}

void qwParseDownload(qwClient_t *qw)
{
	int size, percent;
	char *tempBuf;

	qw->downloadError = true;

	size = rawReadShort(qw->incomingBuffer);
	percent = rawReadByte(qw->incomingBuffer);

	if (size == -1) { //file not found
		qwFinishDownload(qw);
		return;
	}

	if (!qw->download) {
		qw->download = fopen(qw->downloadName, "wb");
		if (!qw->download) {
			rawReadSkip(qw->incomingBuffer, size);
			qwFinishDownload(qw);
			return;
		}
	}

	if (qw->stopDownload) {
		rawReadSkip(qw->incomingBuffer, size);
		qwFinishDownload(qw);
		//printf("DOWNLOAD CANCELED\n");
		return;
	}

	tempBuf = utilMalloc(size);
	rawReadData(qw->incomingBuffer, tempBuf, size);
	fwrite(tempBuf, 1, size, qw->download);
	free(tempBuf);

	printf("%i ", percent);
	if (percent != 100) {
		rawWriteByte(qw->reliableOutgoingBuffer, clc_stringcmd);
		rawWriteString(qw->reliableOutgoingBuffer, "nextdl");
	} else {
		qw->downloadError = false;
		qwFinishDownload(qw);
	}
}

void qwParseServerData(qwClient_t *qw)
{
	while (1) {
		qw->protocolVersion = rawReadLong(qw->incomingBuffer);
		if (qw->protocolVersion == PROTOCOL_VERSION_FTE) {
			qw->fteProtocolExtensions = rawReadLong(qw->incomingBuffer);
			continue;
		}
		if (qw->protocolVersion == PROTOCOL_VERSION_FTE2) {
			qw->fteProtocolExtensions = rawReadLong(qw->incomingBuffer);
			continue;
		}
		if (qw->protocolVersion == PROTOCOL_VERSION)
			break;
	}

	qw->serverCount = rawReadLong(qw->incomingBuffer);
	{
		char gameDir[260+1];
		strcpy(gameDir, utilGetExePath());
		strcat(gameDir, rawReadString(qw->incomingBuffer));
		fsSetDirectory(qw->gameDir, gameDir);
	}

	rawReadByte(qw->incomingBuffer);
	printf("%s\n", rawReadString(qw->incomingBuffer)); //level
	rawReadFloat(qw->incomingBuffer);
	rawReadFloat(qw->incomingBuffer);
	rawReadFloat(qw->incomingBuffer);
	rawReadFloat(qw->incomingBuffer);
	rawReadFloat(qw->incomingBuffer);
	rawReadFloat(qw->incomingBuffer);
	rawReadFloat(qw->incomingBuffer);
	rawReadFloat(qw->incomingBuffer);
	rawReadFloat(qw->incomingBuffer);
	rawReadFloat(qw->incomingBuffer);
	rawWriteChar(qw->reliableOutgoingBuffer, clc_stringcmd);
	rawWriteString(qw->reliableOutgoingBuffer, utilVa("soundlist %i %i", qw->serverCount, 0));
	utilFree(&qw->mapName);
}

void qwParseMessage(qwClient_t *qw)
{
	int		cmd;
	int		i, j, c, lastCmd = 0;
	char	*s;

	while (1) {
		if (!rawCanRead(qw->incomingBuffer))
			break;
		
		cmd = rawReadByte(qw->incomingBuffer);
		if (cmd == -1)
			break;
		printf("[%s]\n", qwGetSvcString(cmd));

		switch (cmd) {
		default:
			printf("illegible server message.\n");
			return;
			
		case svc_nop:
			break;
			
		case svc_disconnect:
			qwDisconnect(qw);			
			break;

		case svc_print:
			rawReadByte(qw->incomingBuffer);
			s = rawReadString(qw->incomingBuffer);
			i = strlen(s);
			if (!i)
				break;
			if (s[i-1] == '\n')
				s[i-1] = 0;
			printf("svc_print: [%s]\n",s);
			
			break;
			
		case svc_centerprint:
			s = rawReadString(qw->incomingBuffer);
			i = strlen(s);
			if (!i)
				break;
			if (s[i-1] == '\n')
				s[i-1] = 0;
			printf("svc_centerprint: [%s]\n", s);
			break;
			
		case svc_stufftext:
			{
				char *s;
						
				s = rawReadString(qw->incomingBuffer);
				printf(">>%s\n", s);
				if (!strncmp(s, "reconnect", 9) || !strncmp(s, "changing", 8)) {
					qw->stopDownload = true;
					rawWriteByte(qw->reliableOutgoingBuffer, clc_stringcmd);
					rawWriteString(qw->reliableOutgoingBuffer, "new");
					break;
				}

				if (!strncmp(s, "cmd new", 7)) {
					printf("sending new...\n");
					rawWriteByte(qw->reliableOutgoingBuffer, clc_stringcmd);
					rawWriteString(qw->reliableOutgoingBuffer, "new");
					break;
				}

				if (!strncmp(s, "cmd pext", 8)) {
					printf("sending protocol extensions...\n");
					rawWriteByte(qw->reliableOutgoingBuffer, clc_stringcmd);
					rawWriteString(qw->reliableOutgoingBuffer, "pext 0x00000000 0x00000000 0x00000000");
					break;
				}

				if (!strncmp(s, "cmd spawn", 9)) {
					printf("sending cmd spawn...\n");
					rawWriteByte(qw->reliableOutgoingBuffer, clc_stringcmd);
					rawWriteString(qw->reliableOutgoingBuffer, s+4);
					break;
				}

				if (!strncmp(s, "cmd prespawn", 12)) {
					printf("sending cmd prespawn...\n");
					rawWriteByte(qw->reliableOutgoingBuffer, clc_stringcmd);
					rawWriteString(qw->reliableOutgoingBuffer, s+4);
					break;
				}

				if (!strcmp(s, "skins\n")) {
					printf("sending begin...\n");
					rawWriteByte(qw->reliableOutgoingBuffer, clc_stringcmd);
					rawWriteString(qw->reliableOutgoingBuffer, utilVa("begin %i", qw->serverCount));
					break;
				}

				//weird new server way of connecting
				if (!strncmp (s, "packet", 6)) {
					char *start;
					char *end;
				
					start = strstr(s, "\"");
					if(!start)
						break;
					start++;
					end = strstr(start, "\"");
					if (!end)
						break;
					*end = 0;

					qwSendConnectionless(qw, start);
					printf("sent %s\n", start);
					*end = '\"';
				
					if (strstr(end, "cmd new")) {
						printf("sending new\n");
						rawWriteByte(qw->reliableOutgoingBuffer, clc_stringcmd);
						rawWriteString(qw->reliableOutgoingBuffer, "new");
					}
				}
			}
			break;
			
		case svc_damage:
			//printf("svc_damage\n");
			rawReadByte(qw->incomingBuffer);
			rawReadByte(qw->incomingBuffer);
			rawReadCoord(qw->incomingBuffer);
			rawReadCoord(qw->incomingBuffer);
			rawReadCoord(qw->incomingBuffer);
			//V_ParseDamage();
			break;
			
		case svc_serverdata:
			qwParseServerData(qw);
			break;
			
		case svc_setangle:
			rawReadAngle(qw->incomingBuffer);
			rawReadAngle(qw->incomingBuffer);
			rawReadAngle(qw->incomingBuffer);
			break;
			
		case svc_lightstyle:
			rawReadByte(qw->incomingBuffer);
			rawReadString(qw->incomingBuffer);
			break;
			
		case svc_sound:
			//printf("svc_sound\n");
			{
				int channel;
				channel = rawReadShort(qw->incomingBuffer);
				if (channel & SND_VOLUME)
					rawReadByte(qw->incomingBuffer);
				if (channel & SND_ATTENUATION)
					rawReadByte(qw->incomingBuffer);
				rawReadByte(qw->incomingBuffer);
				rawReadCoord(qw->incomingBuffer);
				rawReadCoord(qw->incomingBuffer);
				rawReadCoord(qw->incomingBuffer);
			}
			break;
			
		case svc_stopsound:
			//printf("svc_stopsound\n");
			rawReadShort(qw->incomingBuffer);
			break;
		
		case svc_updatefrags:
			//printf("svc_updatefrags\n");
			rawReadByte(qw->incomingBuffer);
			rawReadShort(qw->incomingBuffer);
			break;			

		case svc_updateping:
			//printf("svc_updateping\n");
			rawReadByte(qw->incomingBuffer);
			rawReadShort(qw->incomingBuffer);
			break;
			
		case svc_updatepl:
			//printf("svc_updatepl\n");
			rawReadByte(qw->incomingBuffer);
			rawReadByte(qw->incomingBuffer);
			break;
			
		case svc_updateentertime:
			//printf("svc_updateentertime\n");
			rawReadByte(qw->incomingBuffer);
			rawReadFloat(qw->incomingBuffer);
			break;
			
		case svc_spawnbaseline:
			//printf("svc_spawnbaseline\n");
			rawReadShort(qw->incomingBuffer);
			rawReadByte(qw->incomingBuffer);
			rawReadByte(qw->incomingBuffer);
			rawReadByte(qw->incomingBuffer);
			rawReadByte(qw->incomingBuffer);
			for (i=0 ; i<3 ; i++) {
				rawReadCoord(qw->incomingBuffer);
				rawReadAngle(qw->incomingBuffer);
			}
			break;

		case svc_spawnstatic:
			//printf("svc_spawnstatic\n");
			//rawReadShort();
			rawReadByte(qw->incomingBuffer);
			rawReadByte(qw->incomingBuffer);
			rawReadByte(qw->incomingBuffer);
			rawReadByte(qw->incomingBuffer);
			for (i=0 ; i<3 ; i++) {
				rawReadCoord(qw->incomingBuffer);
				rawReadAngle(qw->incomingBuffer);
			}
			break;			

		case svc_temp_entity:
			//printf("svc_temp_entity\n");

			c = rawReadByte(qw->incomingBuffer);
			if (c == TE_LIGHTNING1 || c == TE_LIGHTNING2 || c == TE_LIGHTNING3) {
				rawReadShort(qw->incomingBuffer);

				rawReadCoord(qw->incomingBuffer);
				rawReadCoord(qw->incomingBuffer);
				rawReadCoord(qw->incomingBuffer);

				rawReadCoord(qw->incomingBuffer);
				rawReadCoord(qw->incomingBuffer);
				rawReadCoord(qw->incomingBuffer);
			}
			else {
				rawReadCoord(qw->incomingBuffer);
				rawReadCoord(qw->incomingBuffer);
				rawReadCoord(qw->incomingBuffer);
			}
			break;

		case svc_killedmonster:
			//printf("svc_killedmonster\n");
			break;

		case svc_foundsecret:
			//printf("svc_foundsecret\n");
			break;

		case svc_updatestat:
			//printf("svc_updatestat\n");
			rawReadByte(qw->incomingBuffer);
			rawReadByte(qw->incomingBuffer);
			break;

		case svc_updatestatlong:
			//printf("svc_updatestatlong\n");
			rawReadByte(qw->incomingBuffer);
			rawReadLong(qw->incomingBuffer);
			break;
			
		case svc_spawnstaticsound:
			//printf("svc_spawnstaticsound\n");
			rawReadCoord(qw->incomingBuffer);
			rawReadCoord(qw->incomingBuffer);
			rawReadCoord(qw->incomingBuffer);
			rawReadByte(qw->incomingBuffer);
			rawReadByte(qw->incomingBuffer);
			rawReadByte(qw->incomingBuffer);
			break;

		case svc_cdtrack:
			//printf("svc_cdtrack\n");
			rawReadByte(qw->incomingBuffer);
			break;

		case svc_intermission:
			//printf("svc_intermission\n");
			rawReadCoord(qw->incomingBuffer);
			rawReadCoord(qw->incomingBuffer);
			rawReadCoord(qw->incomingBuffer);
			rawReadAngle(qw->incomingBuffer);
			rawReadAngle(qw->incomingBuffer);
			rawReadAngle(qw->incomingBuffer);
			break;

		case svc_finale:
			//printf("svc_finale\n");
			rawReadString(qw->incomingBuffer);
			break;
			
		case svc_sellscreen:
			//printf("svc_sellscreen\n");
			break;

		case svc_smallkick:
			//printf("svc_smallkick\n");
			break;

		case svc_bigkick:
			//printf("svc_bigkick\n");
			break;

		case svc_muzzleflash:
			//printf("svc_muzzleflash\n");
			rawReadShort(qw->incomingBuffer);
			break;

		case svc_updateuserinfo:
			//printf("svc_updateuserinfo\n");
			rawReadByte(qw->incomingBuffer);
			rawReadLong(qw->incomingBuffer);
			rawReadString(qw->incomingBuffer);
			break;

		case svc_setinfo:
			//printf("svc_setinfo\n");
			rawReadByte(qw->incomingBuffer);
			rawReadString(qw->incomingBuffer);
			rawReadString(qw->incomingBuffer);
			break;

		case svc_serverinfo:
			//printf("svc_serverinfo\n");
			rawReadString(qw->incomingBuffer);
			rawReadString(qw->incomingBuffer);
			break;

		case svc_download:
			qwParseDownload(qw);
			break;

		case svc_playerinfo:
			//printf("svc_playerinfo\n");
			{
				int flags;
				userCmd_t from, move;

				memset(&from, 0, sizeof(userCmd_t));
				memset(&move, 0, sizeof(userCmd_t));
				rawReadByte(qw->incomingBuffer);
				flags = rawReadShort(qw->incomingBuffer);
				rawReadCoord(qw->incomingBuffer);
				rawReadCoord(qw->incomingBuffer);
				rawReadCoord(qw->incomingBuffer);
				rawReadByte(qw->incomingBuffer);
				if (flags & PF_MSEC)
					rawReadByte(qw->incomingBuffer);
				if (flags & PF_COMMAND)
					qwReadDeltaUsercmd(qw, &from, &move);

				for (i=0 ; i<3 ; i++) {
					if (flags &(PF_VELOCITY1<<i))
						rawReadShort(qw->incomingBuffer);
				}
				if (flags & PF_MODEL)
					rawReadByte(qw->incomingBuffer);
				if (flags & PF_SKINNUM)
					rawReadByte(qw->incomingBuffer);
				if (flags & PF_EFFECTS)
					rawReadByte(qw->incomingBuffer);
				if (flags & PF_WEAPONFRAME)
					rawReadByte(qw->incomingBuffer);
				if (flags & PF_TRANS_Z && qw->fteProtocolExtensions & FTE_PEXT_TRANS)
					rawReadByte(qw->incomingBuffer);
			}
			break;

		case svc_nails:
			//printf("svc_nails\n");
			c = rawReadByte(qw->incomingBuffer);
			for (i=c ; i<c ; i++) {
				for (j=0 ; j<6 ; j++)
					rawReadByte(qw->incomingBuffer);
			}
			break;

		case svc_chokecount:
			printf("svc_chokecount: %i\n", rawReadByte(qw->incomingBuffer));
			//rawReadByte(qw->incomingBuffer);
			break;

		case svc_modellist:
			i = rawReadByte(qw->incomingBuffer);
			for (;;) {
				s = rawReadString(qw->incomingBuffer);
				if (!s[0])
					break;
				//printf("mapname: %i\n", qw->mapName);
				if (!qw->mapName)
					qw->mapName = _strdup(strstr(s, "/")+1);
			}
			i = rawReadByte(qw->incomingBuffer);
			if (i) {
				rawWriteByte(qw->reliableOutgoingBuffer, clc_stringcmd);
				rawWriteString(qw->reliableOutgoingBuffer, utilVa("modellist %i %i", qw->serverCount, i));
				break;
			}

			{
				byte *mapData;
				int mapSize;
				int mapChecksum;

				mapData = qwGetMap(qw, qw->mapName, &mapSize);
				if (!mapData || !mapSize) {
					char download[100];

					sprintf(download, "maps/%s", qw->mapName);
					qwStartDownload(qw, download);
					break;
				}	
				printf("%s\n", qw->mapName);
				mapChecksum = qwCalcMapChecksum(mapData, mapSize);

				qwPreSpawn(qw, mapChecksum);
			}
			break;

		case svc_soundlist:
			i = rawReadByte(qw->incomingBuffer);
			for (;;) {
				if(!(rawReadString(qw->incomingBuffer))[0])
					break;
			}

			i = rawReadByte(qw->incomingBuffer);
			if (i) {
				rawWriteByte(qw->reliableOutgoingBuffer, clc_stringcmd);
				rawWriteString(qw->reliableOutgoingBuffer, utilVa("soundlist %i %i", qw->serverCount, i));
				break;
			}

			//continue login
			rawWriteByte(qw->reliableOutgoingBuffer, clc_stringcmd);
			rawWriteString(qw->reliableOutgoingBuffer, utilVa("modellist %i %i", qw->serverCount, 0));
			break;

		case svc_packetentities:
			//printf("svc_packetentities\n");
			qwFlushEntityPacket(qw);
			break;

		case svc_deltapacketentities:
			//printf("svc_deltapacketentities\n");
			qwFlushEntityPacket(qw);
			break;

		case svc_maxspeed:
			//printf("svc_maxspeed\n");
			rawReadFloat(qw->incomingBuffer);
			break;

		case svc_entgravity:
			//printf("svc_entgravity\n");
			rawReadFloat(qw->incomingBuffer);
			break;

		case svc_setpause:
			//printf("svc_setpause\n");
			rawReadByte(qw->incomingBuffer);
			break;
		case svc_nails2:
			c = rawReadByte(qw->incomingBuffer);
			for (i=c ; i<c ; i++) {
				rawReadByte(qw->incomingBuffer);
				for (j=0 ; j<6 ; j++)
					rawReadByte(qw->incomingBuffer);
			}
			break;
		case svc_fte_modellistshort:
			i = rawReadShort(qw->incomingBuffer);
			for (;;) {
				s = rawReadString(qw->incomingBuffer);
				if (!s[0])
					break;
				if (!qw->mapName)
					qw->mapName = _strdup(strstr(s, "/")+1);
			}
			i = rawReadByte(qw->incomingBuffer);
			if (i) {
				rawWriteByte(qw->reliableOutgoingBuffer, clc_stringcmd);
				rawWriteString(qw->reliableOutgoingBuffer, utilVa("modellist %i %i", qw->serverCount, i));
				break;
			}

			{
				byte *mapData;
				int mapSize;
				int mapChecksum;

				mapData = qwGetMap(qw, qw->mapName, &mapSize);
				if (!mapData || !mapSize) {
					char download[100];

					sprintf(download, "maps/%s", qw->mapName);
					qwStartDownload(qw, download);
					break;
				}	
				printf("%s\n", qw->mapName);
				mapChecksum = qwCalcMapChecksum(mapData, mapSize);

				qwPreSpawn(qw, mapChecksum);
			}
			break;
		case svc_fte_spawnbaseline2:
			{
				entityState_t nullst, es;

				if (!(qw->fteProtocolExtensions & FTE_PEXT_SPAWNSTATIC2)) 
				{
					printf("illegible server message\nsvc_fte_spawnbaseline2 (%i) without FTE_PEXT_SPAWNSTATIC2\n", svc_fte_spawnbaseline2);
					qwDisconnect(qw);
					return;
				}

				memset(&nullst, 0, sizeof (entityState_t));
				memset(&es, 0, sizeof (entityState_t));

				qwParseDelta(qw, &nullst, &es, rawReadShort(qw->incomingBuffer));
			}
			break;
		case svc_qizmovoice:
			// Read the two-byte header.
			rawReadByte(qw->incomingBuffer);
			rawReadByte(qw->incomingBuffer);

			// 32 bytes of voice data follow
			for (i = 0; i < 32; i++)
				rawReadByte(qw->incomingBuffer);
			break;
		case svc_fte_voicechat:
			rawReadByte(qw->incomingBuffer);
			rawReadByte(qw->incomingBuffer);
			rawReadByte(qw->incomingBuffer);
			i = rawReadShort(qw->incomingBuffer);
			rawReadSkip(qw->incomingBuffer, i);
			break;
		case svc_fte_spawnstatic2:
			{
				if (qw->fteProtocolExtensions & FTE_PEXT_SPAWNSTATIC2) {
					entityState_t from, to;
					memset(&from, 0, sizeof(entityState_t));
					memset(&to, 0, sizeof(entityState_t));
					qwParseDelta(qw, &from, &to, rawReadByte(qw->incomingBuffer));
				} 
			}
			break;
		case nq_svc_version:
			rawReadLong(qw->incomingBuffer);
			break;
		case nq_svc_time:
			rawReadFloat(qw->incomingBuffer);
			break;
		case nq_svc_updatename:
			i = rawReadByte(qw->incomingBuffer);
			rawReadString(qw->incomingBuffer);
			break;
		}
		lastCmd = cmd;
	}
}
