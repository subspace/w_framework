#include "object.h"

void objSetApp(void *object, app_t *app)
{
	((object_t *)object)->app = app;
}

app_t *objGetApp(void *object)
{
	return ((object_t *)object)->app;
}

void objSetClientData(void *object, void *clientData)
{
	((object_t *)object)->clientData = clientData;
}

void *objGetClientData(void *object)
{
	return ((object_t *)object)->clientData;
}

