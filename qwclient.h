#ifndef _QWCLIENT_H_
#define _QWCLIENT_H_

#include "app.h"

typedef struct qwClient_s qwClient_t;

qwClient_t *qwNew(app_t *app);
void qwDelete(qwClient_t *qw);
bool qwConnect(qwClient_t *qw, const char *host, ushort port);
bool qwDisconnect(qwClient_t *qw);
bool qwSetGameDir(qwClient_t *qw, const char *dir);
bool qwSetBaseDir(qwClient_t *qw, const char *dir);
void qwSetPing(qwClient_t *qw, ushort ping);

#endif