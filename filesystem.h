#ifndef _FILESYSTEM_H_
#define _FILESYSTEM_H_

#include "list.h"
#include "defines.h"

typedef struct fs_s fs_t;

typedef struct fileInfo_s {
	char			*name;
	char			*fullName;
	bool			isDirectory;
	unsigned long	size;
} fileInfo_t;

fs_t *fsNew(void);
void fsDelete(fs_t *fs);
bool fsSetDirectory(fs_t *fs, const char *directory);
char *fsGetDirectory(fs_t *fs);
list_t *fsGetFileList(fs_t *fs);
byte *fsReadFile(fs_t *fs, fileInfo_t *file);
char *fsFindFileByName(fs_t *fs, const char *fileName);

#endif