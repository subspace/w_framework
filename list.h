#ifndef _LIST_H_
#define _LIST_H_

#include "defines.h"

typedef struct list_s list_t;

#define LIST_BOF	0x01
#define LIST_EOF	0x02
#define LIST_OK		0x04

list_t *listNew(void);
void listDelete(list_t *list);

bool listAdd(list_t *list, void *data);
bool listRemove(list_t *list);
int listMoveNext(list_t *list);
int listMovePrev(list_t *list);
int listMoveFirst(list_t *list);
int listMoveLast(list_t *list);
void *listGetData(list_t *list);
bool listEOF(list_t *list);
bool listBOF(list_t *list);
void listSetOnSelectionChange(list_t *list, void (*func)(list_t *list));
int listGetCount(list_t *list);

#endif