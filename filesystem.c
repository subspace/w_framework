#include "filesystem.h"
#include "util.h"
#include "list.h"
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>

struct fs_s {
	char	*currentDirectory;
	list_t	*fileList;
};

fs_t *fsNew(void)
{
	fs_t *fs;

	fs = utilMalloc(sizeof(fs_t));
	memset(fs, 0, sizeof(fs_t));

	return fs;
}

void fsDelete(fs_t *fs)
{
	free(fs);
}

void fsClearFileList(list_t *fileList)
{
	if (!fileList)
		return;

	for (listMoveFirst(fileList); !listEOF(fileList); listMoveNext(fileList)) {
		struct fileInfo_s *f = listGetData(fileList);
		free(f->name);
		free(f->fullName);
		free(f);
	}
	listDelete(fileList);
}

char *fsGetFilePath(fs_t *fs, fileInfo_t *file)
{
	char *filePath;
	int lenDir, lenFile;

	lenDir = strlen(fs->currentDirectory);
	lenFile = strlen(file->name);
	filePath = utilMalloc(lenDir+lenFile+1);
	memcpy(filePath, fs->currentDirectory, lenDir);
	memcpy(filePath+lenDir, file->name, lenFile+1);

	return filePath;
}

byte *fsReadFile(fs_t *fs, fileInfo_t *file)
{
	byte *buf;
	FILE *f;

	f = fopen(file->fullName, "rb");
	if (!f)
		return NULL;
	buf = utilMalloc(file->size);
	fread(buf, 1, sizeof(byte), f);
	fclose(f);

	return buf;
}

bool fsSetDirectory(fs_t *fs, const char *directory)
{
	WIN32_FIND_DATAA findData;
	HANDLE			hFind;
	int				lenPath;
	char			dirPath[MAX_PATH+1];

	lenPath = strlen(directory);
	if (lenPath < 3)
		return false;

	strcpy(dirPath, directory);

	if (directory[lenPath-1] != '\\')
		strcat(dirPath, "\\");

	//fs->currentDirectory = _strdup(dirPath);
	fs->currentDirectory = _strdup(dirPath);

	strcat(dirPath, "*");

	fsClearFileList(fs->fileList);
	fs->fileList = listNew();

	hFind = FindFirstFileA(dirPath, &findData);
	while (hFind != INVALID_HANDLE_VALUE) {
		struct fileInfo_s *file;

		if (!strcmp(findData.cFileName, ".") || !strcmp(findData.cFileName, ".."))
			goto next_find;

		file = utilMalloc(sizeof(struct fileInfo_s));
		if (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			file->isDirectory = true;
		} else {
			file->isDirectory = false;
		}
		file->name = _strdup(findData.cFileName);
		file->size = findData.nFileSizeLow;
		file->fullName = fsGetFilePath(fs, file);
		listAdd(fs->fileList, file);

next_find:
		if (!FindNextFileA(hFind, &findData))
			break;
	}
	if (hFind != INVALID_HANDLE_VALUE)
		FindClose(hFind);

	return true;
}

list_t *fsGetFileList(fs_t *fs)
{
	return fs->fileList;
}

char *fsGetDirectory(fs_t *fs)
{
	return fs->currentDirectory;
}

char *fsFindFileByName(fs_t *fs, const char *fileName)
{
	static char filePath[261];
	list_t *list = fsGetFileList(fs);
	fileInfo_t *file;

	listMoveFirst(list);
	while (!listEOF(list)) {
		file = listGetData(list);
		if (!file->isDirectory) {
			if (!strcmp(fileName, file->name)) {
				strcpy(filePath, file->fullName);
				return filePath;
			}
		} else {
			fs_t *newDir = fsNew();
			char *found;
			
			if (!fsSetDirectory(newDir, file->fullName)) {
				fsDelete(newDir);
				goto next;
			}

			found = fsFindFileByName(newDir, fileName);
			
			fsDelete(newDir);
			
			if (found)
				return found;
		}
next:
		listMoveNext(list);
	}
	return NULL;
}