#ifndef _UDP_H_
#define _UDP_H_

#include "app.h"
#include "string.h"

#define UDP_UNCONNECTED 0x01
#define UDP_CONNECTED	0x02

typedef struct udp_s udp_t;

udp_t *udpNew(app_t *app);
void udpDelete(udp_t *udp);
int udpSetDestination(udp_t *udp, const char *host, unsigned short port);
int udpSend(udp_t *udp, byte *data, unsigned long size);
int udpSendString(udp_t *udp, const char *data);
int udpRecv(udp_t *udp, byte *data, unsigned long size);
void udpSetOnDataArrival(udp_t *udp, void (*func)(udp_t *udp, byte *data, unsigned long size));
void udpSetOnResolveHost(udp_t *udp, void (*func)(udp_t *udp));
void udpSetOnConnect(udp_t *udp, void (*func)(udp_t *udp));

char *udpGetHost(udp_t *udp);
ushort udpGetPort(udp_t *udp);

#endif