#ifndef _CONSOLE_H_
#define _CONSOLE_H_

#include "app.h"

typedef struct console_s console_t;

console_t *consoleNew(app_t *app);
void consoleDelete(console_t *con);
void consoleSetPrefix(console_t *console, const char *prefix);
void consoleSetOnCommand(console_t *console, void (*func)(console_t *));
void consoleSetOnKeyPress(console_t *console, int (*func)(console_t *, char));
char *consoleArgv(console_t *console, int argNum);
int consoleArgc(console_t *console);

#endif