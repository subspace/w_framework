#include "rawdata.h"
#include "util.h"
#include "object.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

struct rawData_s {
	object_t obj;
	byte	*data;
	ulong	maxSize;
	ulong	size;
	ulong	readCount;
	ulong	writePos;
	bool	badRead;
	bool	badWrite;

	void (*onBadRead)(rawData_t *raw);
	void (*onBadWrite)(rawData_t *raw);
};

rawData_t *rawNew(ulong size)
{
	rawData_t *raw;

	raw = utilMalloc(sizeof(rawData_t));
	memset(raw, 0, sizeof(rawData_t));
	raw->data = utilMalloc(size);
	raw->maxSize = size;

	return raw;
}

void rawDelete(rawData_t *raw)
{
	free(raw->data);
	free(raw);
}

void rawSetOnBadRead(rawData_t *raw, void (*func)(rawData_t *raw))
{
	raw->onBadRead = func;
}

void rawSetOnBadWrite(rawData_t *raw, void (*func)(rawData_t *raw))
{
	raw->onBadWrite = func;
}

void rawClear(rawData_t *raw)
{
	raw->size = 0;
	raw->badRead = false;
	raw->badWrite = false;
	raw->readCount = 0;
	raw->writePos = 0;
}

int rawGetSize(rawData_t *raw)
{
	return raw->size;
}

int rawGetReadCount(rawData_t *raw)
{
	return raw->readCount;
}

int rawGetWritePos(rawData_t *raw)
{
	return raw->writePos;
}

int rawGetMaxSize(rawData_t *raw)
{
	return raw->maxSize;
}

byte *rawGetData(rawData_t *raw)
{
	return raw->data;
}

void rawDebugPrint(rawData_t *raw)
{
	unsigned int i;

	for (i = 0; i < raw->size; i++) {
		if (raw->data[i] < 32 || raw->data[i] > 126)
			printf("0x%1.2x ", raw->data[i]);
		else
			printf("\'%c\' ", raw->data[i]);
	}
}

//
//reading
//
bool rawCanRead(rawData_t *raw)
{
	if (raw->readCount < raw->size)
		return true;
	else
		return false;
}

static bool rawIsBadRead(rawData_t *raw, ushort readSize)
{
	if (raw->readCount + readSize > raw->size || raw->badRead) {
		raw->badRead = true;

		if (raw->onBadRead)
			raw->onBadRead(raw);

		return true;
	}
	return false;
}

void rawSetReadPos(rawData_t *raw, int pos)
{
	if (pos < (signed)raw->size)
		raw->readCount = pos;
	else
		raw->readCount = raw->size;
}

void rawBeginReading(rawData_t *raw)
{
	raw->readCount = 0;
}

char rawReadChar(rawData_t *raw)
{
	if (rawIsBadRead(raw, 1))
		return -1;

	return raw->data[raw->readCount++];
}

byte rawReadByte(rawData_t *raw)
{
	if (rawIsBadRead(raw, 1))
		return -1;

	return (byte)raw->data[raw->readCount++];
}

short rawReadShort(rawData_t *raw)
{
	short r;

	if (rawIsBadRead(raw, 2))
		return -1;

	r = raw->data[raw->readCount] | (raw->data[raw->readCount+1]<<8);
	raw->readCount += 2;

	return r;
}

long rawReadLong(rawData_t *raw)
{
	long r;

	if (rawIsBadRead(raw, 4))
		return -1;

	r = raw->data[raw->readCount] |
		(raw->data[raw->readCount+1]<<8) |
		(raw->data[raw->readCount+2]<<16) |
		(raw->data[raw->readCount+3]<<24);

	raw->readCount += 4;

	return r;
}

float rawReadFloat(rawData_t *raw)
{
	union {
		byte b[4];
		float f;
		int l;
	} dat;

	if (rawIsBadRead(raw, 4))
		return -1;

	dat.b[0] = raw->data[raw->readCount];
	dat.b[1] = raw->data[raw->readCount+1];
	dat.b[2] = raw->data[raw->readCount+2];
	dat.b[3] = raw->data[raw->readCount+3];

	dat.l = utilLittleLong(dat.l);
	raw->readCount += 4;

	return dat.f;
}

char *rawReadString(rawData_t *raw)
{
	static char	buf[1024];
	int		bufSize;
	int		i;
	char	c;

	bufSize = raw->size - raw->readCount;
	if (bufSize < 0) {
		raw->badRead = true;
		return NULL;
	}

	//buf = utilMalloc(bufSize+1);
	for (i = 0; i < bufSize; i++) {
		c = rawReadByte(raw);
		if (c == -1 || !c)
			break;
		buf[i] = c;
	}
	buf[i] = 0;
	
	return buf;
}

char *rawReadStringLine(rawData_t *raw)
{
	static char	buf[1024];
	int		bufSize;
	int		i;
	char	c;

	bufSize = raw->size - raw->readCount;
	if (bufSize < 0) {
		raw->badRead = true;
		return NULL;
	}

	//buf = utilMalloc(bufSize+1);
	for (i = 0; i < bufSize; i++) {
		c = rawReadByte(raw);
		if (c == -1 || !c || c == '\n')
			break;
		buf[i] = c;
	}
	buf[i++] = 0;

	return buf;
}

void rawReadData(rawData_t *raw, void *data, int len)
{
	int i;

	for (i = 0; i < len; i++)
		((byte *)data)[i] = rawReadByte(raw);
}

void rawReadSkip(rawData_t *raw, int bytes)
{
	while (!raw->badRead && bytes > 0) {
		rawReadByte(raw);
		bytes--;
	}
}

//
// writing
//
static bool rawIsBadWrite(rawData_t *raw, int len)
{
	if (raw->writePos+len > raw->maxSize || raw->badWrite) {
		raw->badWrite = true;
		if (raw->onBadWrite)
			raw->onBadWrite(raw);

		return true;
	}
	return false;
}

static void rawUpdateSize(rawData_t *raw)
{
	if (raw->writePos > raw->size)
		raw->size = raw->writePos;
}

void rawWriteChar(rawData_t *raw, int c)
{
	if (rawIsBadWrite(raw, 1))
		return;
	raw->data[raw->writePos++] = c;
	rawUpdateSize(raw);
}

void rawWriteByte(rawData_t *raw, int c)
{
	rawWriteChar(raw, c);
}

void rawWriteShort(rawData_t *raw, int c)
{
	if (rawIsBadWrite(raw, 2))
		return;

	raw->data[raw->writePos] = c & 0xff;
	raw->data[raw->writePos+1] = c>>8;
	raw->writePos += 2;

	rawUpdateSize(raw);
}

void rawWriteLong(rawData_t *raw, int c)
{
	if (rawIsBadWrite(raw, 4))
		return;

	raw->data[raw->writePos] = c & 0xff;
	raw->data[raw->writePos+1] = (c>>8) & 0xff;
	raw->data[raw->writePos+2] = (c>>16) & 0xff;
	raw->data[raw->writePos+3] = (c>>24) & 0xff;

	raw->writePos += 4;

	rawUpdateSize(raw);
}

void rawWriteFloat(rawData_t *raw, float f)
{
	union {
		float f;
		int l;
	} dat;
	
	if (rawIsBadWrite(raw, 4))
		return;

	dat.f = f;
	dat.l = utilLittleLong(dat.l);
	
	*(raw->data+raw->writePos) = dat.l;
	raw->writePos += 4;

	rawUpdateSize(raw);
}

void rawWriteString(rawData_t *raw, char *s)
{
	int len = strlen(s)+1;

	if (rawIsBadWrite(raw, len))
		return;
	memcpy(&raw->data[raw->writePos], s, len);
	raw->writePos += len;

	rawUpdateSize(raw);
}

void rawWrite(rawData_t *raw, void *data, int len)
{
	if (rawIsBadWrite(raw, len))
		return;

	memcpy(&raw->data[raw->writePos], data, len);
	raw->writePos += len;

	rawUpdateSize(raw);
}

void rawWriteRaw(rawData_t *dest, rawData_t *org)
{
	if (rawIsBadWrite(dest, org->size))
		return;

	memcpy(dest->data+dest->writePos, org->data, org->size);
	//printf("[%s]", dest->data+dest->writePos);
	dest->writePos += org->size;
	rawUpdateSize(dest);
}

bool rawCanWrite(rawData_t *raw)
{
	if (raw->writePos < raw->maxSize)
		return true;
	else
		return false;
}