#include "qwclient.h"
#include "qwclient_local.h"
#include "udp.h"
#include "filesystem.h"
#include "object.h"
#include "timer.h"
#include "rawdata.h"
#include "util.h"
#include "pack.h"
#include "mdfour.h"
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <string.h>


void qwHostError(rawData_t *raw)
{
	qwClient_t *qw = objGetClientData(raw);
	printf("bad read client disconnected.\n");
	rawDebugPrint(raw);
	qwDisconnect(qw);
}

qwClient_t *qwNew(app_t *app)
{
	qwClient_t *qw;

	qw = utilMalloc(sizeof(qwClient_t));
	memset(qw, 0, sizeof(qwClient_t));

	objSetApp(qw, app);

	//setup UDP interface
	qw->server = udpNew(app);
	objSetClientData(qw->server, qw);
	

	//setup timer
	qw->timer = timerNew(app);
	objSetClientData(qw->timer, qw);
	timerSetFrequency(qw->timer, 5000);
		
	qw->state = QW_DISCONNECTED;
	
	//setup buffers
	qw->reliableOutgoingBuffer = rawNew(MAX_MSGLEN);
	qw->unreliableOutgoingBuffer = rawNew(MAX_MSGLEN);
	qw->incomingBuffer = rawNew(MAX_MSGLEN);
	qw->reliableBuffer = rawNew(MAX_MSGLEN);
	qw->sendBuffer = rawNew(MAX_MSGLEN);

	//gamedir
	qw->gameDir = fsNew();
	qw->baseDir = fsNew();

	objSetClientData(qw->incomingBuffer, qw);
	rawSetOnBadRead(qw->incomingBuffer, qwHostError);

	qw->qPort = ((int)(timerGetTicks()*1000) * time(NULL)) & 0xffff;
	qw->ping = 100;
	
	return qw;
}

bool qwSetBaseDir(qwClient_t *qw, const char *dir)
{
	list_t *files;

	if (!qw->baseDir)
		qw->baseDir = fsNew();
	
	if (!fsSetDirectory(qw->baseDir, dir))
		return false;

	if (!qw->packFiles)
		qw->packFiles = listNew();
	
	//load all pack files in baseDirectory;
	files = fsGetFileList(qw->baseDir);
	listMoveFirst(files);
	while (!listEOF(files)) {
		fileInfo_t *f;
		f = listGetData(files);
		if (strstr(f->name, ".pak")) {
			pack_t *pack = packNew();
			if (!packLoad(pack, f->fullName))
				packDelete(pack);
			else
				listAdd(qw->packFiles, pack);
		}
		listMoveNext(files);
	}
	listDelete(files);

	return true;
}

#include <direct.h>
bool qwSetGameDir(qwClient_t *qw, const char *dir)
{
	fs_t *f = fsNew();
	char newDir[261];

	if (!qw->gameDir)
		qw->gameDir = fsNew();
	
	if (!fsSetDirectory(qw->gameDir, dir))
		_mkdir(dir);
	
	//create maps directory
	if (dir[strlen(dir)-1] == '\\')
		sprintf(newDir, "%smaps", dir);
	else
		sprintf(newDir, "%s\\maps", dir);
	
	if (!fsSetDirectory(f, newDir))
		_mkdir(newDir);

	fsDelete(f);

	return fsSetDirectory(qw->gameDir, dir);
}

byte *qwGetMap(qwClient_t *qw, const char *map, int *outSize)
{
	//look inside gameDir first
	if (qw->gameDir) {
		char *mapPath = fsFindFileByName(qw->gameDir, map);
		if (mapPath) {
			FILE *f;
			byte *buf;

			f = fopen(mapPath, "rb");
			fseek(f, 0, SEEK_END);
			*outSize = ftell(f);
			fseek(f, 0, SEEK_SET);
			buf = utilMalloc(*outSize);
			fread(buf, 1, *outSize, f);
			fclose(f);
			return buf;
		}
	}

	//look inside loaded packfiles
	if (qw->packFiles) {
		listMoveFirst(qw->packFiles);
		while (!listEOF(qw->packFiles)) {
			pack_t *pack;
			packFile_t *files;
			int numFiles;

			pack = listGetData(qw->packFiles);
			files = packGetFiles(pack, &numFiles);
			if (!numFiles) {
				listMoveNext(qw->packFiles);
				continue;
			}
			while (numFiles--) {
				if (!_stricmp(files[numFiles].name, map))
					return packReadFile(pack, &files[numFiles]);
			}
			listMoveNext(qw->packFiles);
		}
	}

	return NULL;
}

void qwSendMovement(timer_t *timer)
{
	qwClient_t *qw = objGetClientData(timer);
	
	rawWriteByte(qw->unreliableOutgoingBuffer, clc_move);
	rawWriteByte(qw->unreliableOutgoingBuffer, 0x10);
	rawWriteByte(qw->unreliableOutgoingBuffer, 0x02);
	rawWriteByte(qw->unreliableOutgoingBuffer, 0x00);
	rawWriteByte(qw->unreliableOutgoingBuffer, 0x00);
	rawWriteByte(qw->unreliableOutgoingBuffer, 0x00);
	rawWriteByte(qw->unreliableOutgoingBuffer, 0x22);
	rawWriteByte(qw->unreliableOutgoingBuffer, 0x00);
	rawWriteByte(qw->unreliableOutgoingBuffer, 0x21);
	qwSendPacket(qw);
}

int qwCallback(qwClient_t *qw)
{
	return 0;
}

void qwDelete(qwClient_t *qw)
{
	qwDisconnect(qw);
	
	//destroy udp interface
	udpDelete(qw->server);

	//destroy buffers
	rawDelete(qw->reliableOutgoingBuffer);
	rawDelete(qw->unreliableOutgoingBuffer);
	rawDelete(qw->incomingBuffer);
	rawDelete(qw->reliableBuffer);
	rawDelete(qw->sendBuffer);

	//unload packfiles and destroy list
	if (qw->packFiles) {
		listMoveFirst(qw->packFiles);
		while (!listEOF(qw->packFiles)) {
			packDelete(listGetData(qw->packFiles));
			listMoveNext(qw->packFiles);
		}
		listDelete(qw->packFiles);
	}

	if (qw->gameDir)
		fsDelete(qw->gameDir);

	if (qw->baseDir)
		fsDelete(qw->baseDir);

	free(qw);
}


void qwSendConnectionless(qwClient_t *qw, byte *data)
{
	char packet[1024];

	sprintf(packet, "����%s", data);
	udpSend(qw->server, packet, strlen(packet));
}

void qwGetChallenge(timer_t *timer)
{
	qwClient_t *qw;

	qw = objGetClientData(timer);

	if (qw->retryTimes < 3) {
		qwSendConnectionless(qw, "getchallenge\n");
		qw->retryTimes++;
	} else {
		qwDisconnect(qw);
	}
}

void qwSendPacket(qwClient_t *qw)
{
	bool sendReliable;

	sendReliable = false;
	if (qw->incomingAcknowledged > qw->lastReliableSequence && qw->incomingAcknowledgedReliable != qw->outgoingReliable)
		sendReliable = true;

	if (!rawGetSize(qw->reliableBuffer) && rawGetSize(qw->reliableOutgoingBuffer)) {
		rawBeginReading(qw->reliableOutgoingBuffer);
		rawWriteRaw(qw->reliableBuffer, qw->reliableOutgoingBuffer);
		
		qw->outgoingReliable ^= 1;
		sendReliable = true;
		rawClear(qw->reliableOutgoingBuffer);
	}


	rawClear(qw->sendBuffer);
	rawWriteLong(qw->sendBuffer, qw->outgoingSequence | (sendReliable << 31));
	rawWriteLong(qw->sendBuffer, qw->incomingSequence | (qw->incomingReliable << 31));
	rawWriteShort(qw->sendBuffer, qw->qPort);
	qw->outgoingSequence++;

	if (sendReliable) {
		rawWriteRaw(qw->sendBuffer, qw->reliableBuffer);
		qw->lastReliableSequence = qw->outgoingSequence;
	}
	if (rawGetSize(qw->sendBuffer) + rawGetSize(qw->unreliableOutgoingBuffer) < rawGetMaxSize(qw->sendBuffer))
		rawWriteRaw(qw->sendBuffer, qw->unreliableOutgoingBuffer);
	rawClear(qw->unreliableOutgoingBuffer);
	
	if (rawGetSize(qw->sendBuffer))
		udpSend(qw->server, rawGetData(qw->sendBuffer), rawGetSize(qw->sendBuffer));
}
	
void qwSendConnect(timer_t *timer)
{
	qwClient_t *qw;

	qw = objGetClientData(timer);

	if (qw->retryTimes < 3) {
		char connectString[1024];
		
		sprintf(connectString, "connect %i %i %i \"\\rate\\2500\\msg\\1\\noaim\\1\\topcolor\\0\\bottomcolor\\0\\w_switch\\2\\b_switch\\2\\*client\\ezQuake 2925\\name\\qw\\team\\qwBrasilBot\\spectator\\1\\pmodel\\33168\\emodel\\6967\\*z_ext\\383\"", PROTOCOL_VERSION, qw->qPort, qw->challenge);
		qwSendConnectionless(qw, connectString);
		qw->retryTimes++;
	} else {
		qwDisconnect(qw);
	}
}

void qwProcessPacket(qwClient_t *qw)
{
	unsigned long incomingSequence;
	unsigned long incomingAcknowledged;
	bool incomingReliable;
	bool incomingAcknowledgedReliable;

	rawBeginReading(qw->incomingBuffer);

	incomingSequence = rawReadLong(qw->incomingBuffer);
	incomingAcknowledged = rawReadLong(qw->incomingBuffer);

	incomingReliable = incomingSequence >> 31;
	incomingAcknowledgedReliable = incomingAcknowledged >> 31;
	
	incomingSequence &= ~0x80000000;
	incomingAcknowledged &= ~0x80000000;
#if 0
	printf ("<-- s=%i(%i) a=%i(%i) %i\n"
		            , incomingSequence
		            , incomingReliable
		            , incomingAcknowledged
		            , incomingAcknowledgedReliable
					, rawGetSize(qw->incomingBuffer));
#endif
	if (incomingSequence <= qw->incomingSequence) //drop stale or duplicate packets
		return;

	if ((incomingSequence - (qw->incomingSequence+1)) > 0) //packets dropped
		qw->dropCount++;

	if (incomingAcknowledgedReliable == qw->outgoingReliable) {
		//printf("cleaning reliable buffer.");
		rawClear(qw->reliableBuffer);
	}

	qw->incomingSequence = incomingSequence;
	if (incomingReliable)
		qw->incomingReliable ^= 1;

	qw->incomingAcknowledged = incomingAcknowledged;
	qw->incomingAcknowledgedReliable = incomingAcknowledgedReliable;
}



int qwCalcMapChecksum(byte *data, int size)
{
	dheader_t		*header;
	unsigned int	checksum;
	unsigned char	*mod_base;
	int i;

	header = (dheader_t *)data;
	mod_base = (unsigned char *)data;

	checksum = 0;

	for (i = 0 ; i < HEADER_LUMPS ; i++) {
		if (i == LUMP_ENTITIES || i == LUMP_VISIBILITY || i == LUMP_LEAFS || i == LUMP_NODES)
			continue;
		checksum ^= Com_BlockChecksum (mod_base + header->lumps[i].fileofs,	header->lumps[i].filelen);
	}
	return checksum;
}

void qwPreSpawn(qwClient_t *qw, int mapChecksum)
{
	//setinfo pmodel emodel
	rawWriteByte(qw->reliableOutgoingBuffer, clc_stringcmd);
	rawWriteString(qw->reliableOutgoingBuffer, "setinfo pmodel 33168");
	rawWriteByte(qw->reliableOutgoingBuffer, clc_stringcmd);
	rawWriteString(qw->reliableOutgoingBuffer, "setinfo emodel 6967");
	rawWriteByte(qw->reliableOutgoingBuffer, clc_stringcmd);
	rawWriteString(qw->reliableOutgoingBuffer, utilVa("prespawn %i 0 %i", qw->serverCount, mapChecksum));
}

void qwStartDownload(qwClient_t *qw, const char *fileName)
{
	char *downloadName = utilMalloc(260+1);
	ushort i;

	qw->stopDownload = false;

	printf("starting download %s\n", fileName);
	rawWriteByte(qw->reliableOutgoingBuffer, clc_stringcmd);
	rawWriteString(qw->reliableOutgoingBuffer, utilVa("download %s", fileName));

	//generate disk path
	sprintf(downloadName, "%s%s.tmp", fsGetDirectory(qw->gameDir), fileName);
	
	//invert bars
	for (i = 0; i < strlen(downloadName); i++) {
		if (downloadName[i] == '/')
			downloadName[i] = '\\';
	}
	qw->downloadName = downloadName;

	timerSetFrequency(qw->timer, 20); //full speed
}

void qwFinishDownload(qwClient_t *qw)
{
	printf("download finished %s\n", qw->downloadName);
	if (qw->download) {
		fclose(qw->download);
		qw->download = NULL;
	}
	if (!qw->downloadError) {
		char newName[261];
		char *p;
		int size;
		byte *map;

		strcpy(newName, qw->downloadName);
		p = newName+strlen(newName)-1;
		while (*p != '.' && p != newName)
			p--;
		*p = 0;
		rename(qw->downloadName, newName);
		printf(">>>>>>>>>%s\n", qw->mapName);
		map = qwGetMap(qw, qw->mapName, &size);
		if (!map) {
			printf("shouldn't happen");
			qwDisconnect(qw);
		}

		//continue login
		qwPreSpawn(qw, qwCalcMapChecksum(map, size));
		utilFree(&map);
	} else {
		qwDisconnect(qw);
		printf("no map");
	}
	utilFree(&qw->downloadName);
	timerSetFrequency(qw->timer, qw->ping);
}


void qwOnDataArrival(udp_t *udp, byte *data, ulong size)
{
	qwClient_t	*qw;
	char		*challengeString;

	qw = objGetClientData(udp);
	
	rawClear(qw->incomingBuffer);
	rawWrite(qw->incomingBuffer, data, size);

	switch (qw->state) {
	case QW_GETCHALLENGE:
		if (rawReadLong(qw->incomingBuffer) != -1) //this is not a connectionless msg
			break;
		if (rawReadChar(qw->incomingBuffer) != S2C_CHALLENGE) //this is not what we are waiting for
			break;

		challengeString = rawReadString(qw->incomingBuffer);

		qw->challenge = atol(challengeString);
		qw->state = QW_CONNECT;

		qw->retryTimes = 0;
		timerSetOnTimeout(qw->timer, qwSendConnect);
		qwSendConnect(qw->timer);
		//printf("sent connect");
		qw->downloadError = false;
		break;

	case QW_CONNECT:
		if (rawReadLong(qw->incomingBuffer) != -1)
			break;
		if (rawReadChar(qw->incomingBuffer) != S2C_CONNECTION)
			break;
		qw->state = QW_CONNECTED;

		qw->retryTimes = 0;
		
		rawWriteChar(qw->reliableOutgoingBuffer, clc_stringcmd);
		rawWriteString(qw->reliableOutgoingBuffer, "new");
		//qwSendPacket(qw);
		appAddCallback(objGetApp(qw), qwCallback, qw);
		timerSetOnTimeout(qw->timer, qwSendMovement);
		timerSetFrequency(qw->timer, qw->ping);
		timerStart(qw->timer);
		//timerStop(qw->timer);
		break;

	default:
		qwProcessPacket(qw);
		qwParseMessage(qw);
		break;
	}
}

bool qwConnect(qwClient_t *qw, const char *host, ushort port)
{
	if (qw->state != QW_DISCONNECTED)
		return false;

	if (!udpSetDestination(qw->server, host, port))
		return false;
	
	udpSetOnDataArrival(qw->server, qwOnDataArrival);
	
	qw->retryTimes = 0;
	timerSetOnTimeout(qw->timer, qwGetChallenge);
	timerStart(qw->timer);

	qw->state = QW_GETCHALLENGE;
	qwGetChallenge(qw->timer);
	qw->outgoingSequence = 1;

	return true;
}

void qwCancelDownload(qwClient_t *qw)
{
	utilFree(&qw->mapName);
	utilFree(&qw->downloadName);
	if (qw->download) {
		fclose(qw->download);
		qw->download = 0;
	}
}

bool qwDisconnect(qwClient_t *qw)
{
	//send drop 3x to make sure somehow =P
	rawWriteByte(qw->unreliableOutgoingBuffer, clc_stringcmd);
	rawWriteString(qw->unreliableOutgoingBuffer, "drop");
	qwSendPacket(qw);

	rawWriteByte(qw->unreliableOutgoingBuffer, clc_stringcmd);
	rawWriteString(qw->unreliableOutgoingBuffer, "drop");
	qwSendPacket(qw);

	rawWriteByte(qw->unreliableOutgoingBuffer, clc_stringcmd);
	rawWriteString(qw->unreliableOutgoingBuffer, "drop");
	qwSendPacket(qw);

	timerSetFrequency(qw->timer, qw->ping);
	timerStop(qw->timer);

	qwCancelDownload(qw);

	qw->retryTimes = 0;

	rawClear(qw->reliableOutgoingBuffer);
	rawClear(qw->unreliableOutgoingBuffer);
	rawClear(qw->reliableBuffer);
	rawClear(qw->sendBuffer);
	rawClear(qw->incomingBuffer);

	udpSetOnDataArrival(qw->server, NULL);
	
	appRemoveCallback(objGetApp(qw), qwCallback, qw);
	
	return true;
}

void qwSetPing(qwClient_t *qw, ushort ping)
{
	qw->ping = ping;
	timerSetFrequency(qw->timer, ping);
}

