#ifndef _PACK_H_
#define _PACK_H_

#include "defines.h"

typedef struct pack_s pack_t;

typedef struct
{
	char	*name;
	char	*fullName;
	int		filePos, fileLen;
} packFile_t;

pack_t *packNew(void);
void packDelete(pack_t *pack);

bool packLoad(pack_t *pack, const char *path);
bool packUnload(pack_t *pack);
packFile_t *packGetFiles(pack_t *pack, int *numFiles);

byte *packReadFile(pack_t *pack, packFile_t *file);

#endif