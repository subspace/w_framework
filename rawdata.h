#ifndef _RAWDATA_H_
#define _RAWDATA_H_

#include "defines.h"

typedef struct rawData_s rawData_t;

rawData_t *rawNew(ulong size);
void rawDelete(rawData_t *raw);
void rawSetOnBadRead(rawData_t *raw, void (*func)(rawData_t *raw));
void rawSetOnBadWrite(rawData_t *raw, void (*func)(rawData_t *raw));
void rawClear(rawData_t *raw);
int rawGetSize(rawData_t *raw);
int rawGetReadCount(rawData_t *raw);
int rawGetMaxSize(rawData_t *raw);
byte *rawGetData(rawData_t *raw);
void rawDebugPrint(rawData_t *raw);

//
// writing functions
//
bool rawCanWrite(rawData_t *raw);
void rawWriteChar(rawData_t *raw, int c);
void rawWriteByte(rawData_t *raw, int c);
void rawWriteShort(rawData_t *raw, int c);
void rawWriteLong(rawData_t *raw, int c);
void rawWriteFloat(rawData_t *raw, float f);
void rawWriteString(rawData_t *raw, char *s);
void rawWrite(rawData_t *raw, void *data, int len);
void rawWriteRaw(rawData_t *dest, rawData_t *org);

void rawSetWritePos(rawData_t *raw, int pos);

//
// reading functions
//
bool rawCanRead(rawData_t *raw);
void rawBeginReading(rawData_t *raw);
char rawReadChar(rawData_t *raw);
byte rawReadByte(rawData_t *raw);
short rawReadShort(rawData_t *raw);
long rawReadLong(rawData_t *raw);
float rawReadFloat(rawData_t *raw);
char *rawReadString(rawData_t *raw);
char *rawReadStringLine(rawData_t *raw);
void rawReadData(rawData_t *raw, void *data, int len);
void rawReadSkip(rawData_t *raw, int bytes);
void *rawReadAll(rawData_t *raw);
void rawRead(rawData_t *raw);
void rawSetReadPos(rawData_t *raw, int pos);

#endif