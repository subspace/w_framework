#include "parser.h"
#include "list.h"
#include "util.h"

#include "parser_local.h"

#include <string.h>

parser_t *parserNew(void)
{
	parser_t *parser = utilMalloc(sizeof(parser_t));
}

void parserFreeLineFields(struct parserLine_s *line)
{
	struct parserField_s *field;

	if (!line->fields)
		return;

	listMoveFirst(line->fields);
	while (!listEOF(line->fields)) {
		field = listGetData(line->fields);
		listMoveNext(line->fields);
	}
	listDelete(line->fields);
}

void parserFreeLines(parser_t *parser)
{
	struct parserLine_s *line;

	if (!parser->lines)
		return;

	listMoveFirst(parser->lines);
	while (!listEOF(parser->lines)) {
		line = listGetData(parser->lines);
		
		parserFreeLineFields(line);

		listMoveNext(parser->lines);
	}
	listDelete(parser->lines);
}

void parserFreeParsedFields(struct parsedLine_s *line)
{
	struct parsedField_s *field;

	if (!line->parsedFields)
		return;

	listMoveFirst(line->parsedFields);
	while (!listEOF(line->parsedFields)) {
		field = listGetData(line->parsedFields);

		utilFree(&field->value);
		
		listMoveNext(line->parsedFields);
	}
	listDelete(line->parsedFields);
}

void parserFreeParsedLines(parser_t *parser)
{
	struct parsedLine_s *line;

	if (!parser->parsedLines)
		return;

	listMoveFirst(parser->parsedLines);
	while (!listEOF(parser->parsedLines)) {
		line = listGetData(parser->parsedLines);

		parserFreeParsedFields(line);

		listMoveNext(parser->parsedLines);
	}
	listDelete(parser->parsedLines);
}

void parserDelete(parser_t *parser)
{
	parserFreeLines(parser);
	parserFreeParsedLines(parser);
}

struct parserLine_s *parserFindLine(parser_t *parser, const char *lineName)
{
	struct parserLine_s *line;

	if (!parser->lines)
		return NULL;

	listMoveFirst(parser->lines);
	while (!listEOF(parser->lines)) {
		line = listGetData(parser->lines);
		if (!strcmp(line->name, lineName))
			return line;
		listMoveNext(parser->lines);
	}
	return NULL;
}

void parserAddLine(parser_t *parser, const char *name, int size, const char *delimiter)
{
	struct parserLine_s *line;

	if (!parser->lines)
		parser->lines = listNew();
	
	if (parserFindLine(parser, name))
		return;

	line = utilMalloc(sizeof(struct parserLine_s));
	line->name = _strdup(name);
	line->size = size;
	if (delimiter)
		line->delimiter = _strdup(delimiter);
	else
		line->delimiter = NULL;
}

void parserAddField(parser_t *parser, const char *lineName, const char *name, int size, const char *delimiter)
{
	struct parserField_s *field;
	struct parserLine_s *line;

	line = parserFindLine(parser, lineName);
	if (!line)
		return;

	if (!line->fields)
		line->fields = listNew();
	

}