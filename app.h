#ifndef _APP_H_
#define _APP_H_

#include "defines.h"

typedef struct app_s app_t;

extern bool winsockEnabled;

app_t *appNew(void);
void appDelete(app_t *app);

int appMainLoop(app_t *app);
int appRun(app_t *app);
bool appAddCallback(app_t *app, int (*callback)(void *object), void *object);
bool appRemoveCallback(app_t *app, int (*callback)(void *object), void *object);
bool appEnableWinsock(void);
bool appDisableWinsock(void);

#endif