#include "object.h"
#include "console.h"
#include "util.h"
#include "list.h"
#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define CONSOLE_BUFFER_SIZE 1024
#define CONSOLE_MAX_ARGS	32

typedef struct console_s {
	object_t obj;
	list_t	*buffers;
	char	*actualBuffer;
	int		bufferPos;
	int		bufferStringSize;
	char	*argv[CONSOLE_MAX_ARGS];
	int		argc;
	char	*prefix;
	bool	firstLoop;

	int		(*onKeyPress)(console_t *console, char key);
	void	(*onCommand)(console_t *console);
};

char *consoleSkipWhiteSpace(char *buffer)
{
	while (*buffer == ' ' || *buffer == '\t' || *buffer == '\n' || *buffer == '\r')
		buffer++;
	return buffer;
}

char *consoleSkipWord(char *buffer)
{
	while ((*buffer != ' ' && *buffer != '\t' && *buffer != '\n' && *buffer != '\r') && *buffer)
		buffer++;
	return buffer;
}

char *consoleSkipQuotedWord(char *buffer)
{
	while (*buffer != '\"' && *buffer != '\n' && *buffer != '\r' && *buffer)
		buffer++;

	//if (*buffer == '\"')
	//	buffer++;

	return buffer;
}

void consoleSetPrefix(console_t *console, const char *prefix)
{
	if (console->prefix)
		free(console->prefix);
	console->prefix = _strdup(prefix);
	//printf("%s", console->prefix);
}

void consoleFreeArgs(console_t *console)
{
	int i;

	for (i = 0; console->argv[i]; i++) {
		free(console->argv[i]);
		console->argv[i] = 0;
	}
	console->argc = 0;
}

void consoleTokenizeBuffer(console_t *console)
{
	char *buf, *start, c;
	bool quoted;

	consoleFreeArgs(console);

	buf = console->actualBuffer;
	while (1) {
		buf = consoleSkipWhiteSpace(buf);
		if (!*buf)
			break;

		if (*buf == '\"') {
			quoted = true;
			buf++;
		} else {
			quoted = false;
		}

		start = buf;
		
		if (quoted)
			buf = consoleSkipQuotedWord(buf);
		else
			buf = consoleSkipWord(buf);

		c = *buf;
		*buf = 0;
		console->argv[console->argc++] = _strdup(start);
		*buf = c;
		
		if (!*buf)
			break;
		buf++;

		if (console->argc > CONSOLE_MAX_ARGS-1)
			break;
	}
}
	
void consoleSetNewBuffer(console_t *console)
{
	if (console->actualBuffer)
		listAdd(console->buffers, console->actualBuffer);

	console->actualBuffer = utilMalloc(CONSOLE_BUFFER_SIZE);
	console->bufferStringSize = console->bufferPos = 0;
}

void consoleClearCurrentBuffer(console_t *console)
{
	int i;

	for (i = 0; i < console->bufferPos; i++)
		printf("\b \b");
	console->actualBuffer[0] = 0;
}

void consoleSetOnCommand(console_t *console, void (*func)(console_t *))
{
	console->onCommand = func;
}

void consoleOnKeyPress(console_t *console, char key)
{
	if (console->onKeyPress)
		if (console->onKeyPress(console, key))
			return;

	switch(key) {
		case '\n':
		case '\r':
			if (console->bufferStringSize == 0) {
				_putch('\n');
				if (console->prefix)
					printf("%s", console->prefix);
				break;
			}
			console->actualBuffer[console->bufferStringSize] = 0;
			consoleTokenizeBuffer(console);
			consoleSetNewBuffer(console);
			listMoveLast(console->buffers);
			_putch('\n');
			
			if (console->onCommand)
				console->onCommand(console);

			if (console->prefix)
				printf("%s", console->prefix);
			break;

		case '\b':
			console->bufferPos--;
			console->bufferStringSize--;
			_putch('\b \b');
			break;

		case -32:
			switch(_getch()) {
			case 72: //up
				if (!listBOF(console->buffers) && !listEOF(console->buffers)) {
					consoleClearCurrentBuffer(console);
					strcpy(console->actualBuffer, listGetData(console->buffers));
					printf("%s", console->actualBuffer);
					console->bufferStringSize = console->bufferPos = strlen(console->actualBuffer);
					listMovePrev(console->buffers);
				}
				if (listBOF(console->buffers))
					listMoveFirst(console->buffers);
				break;
			case 80: //down
				listMoveNext(console->buffers);
				if (!listEOF(console->buffers) && !listBOF(console->buffers)) {
					consoleClearCurrentBuffer(console);
					strcpy(console->actualBuffer, listGetData(console->buffers));
					printf("%s", console->actualBuffer);
					console->bufferStringSize = console->bufferPos = strlen(console->actualBuffer);
				}
				if (listEOF(console->buffers))
					listMoveLast(console->buffers);

				break;
			case 75: //left
				if (console->bufferPos > 0) {
					_putch('\b');
					console->bufferPos--;
				}
				break;
			case 77: //right
				if (console->bufferPos < console->bufferStringSize)
					_putch(console->actualBuffer[console->bufferPos++]);
				break;
			}
			break;
		default:
			if (console->bufferStringSize < CONSOLE_BUFFER_SIZE-1) {
				console->actualBuffer[console->bufferPos++] = key;
				console->bufferStringSize++;
				_putch(key);
			}
			break;
	}
}

int consoleCallback(console_t *console)
{
	if (console->firstLoop) {
		console->firstLoop = false;
		if (console->prefix)
			printf("%s", console->prefix);
	}

	while (_kbhit()) {
		consoleOnKeyPress(console, _getch());
	}
	return 0;
}

console_t *consoleNew(app_t *app)
{
	console_t *con;

	con = utilMalloc(sizeof(console_t));
	memset(con, 0, sizeof(console_t));
	objSetApp(con, app);
	con->buffers = listNew();
	consoleSetNewBuffer(con);
	con->firstLoop = true;
	appAddCallback(objGetApp(con), consoleCallback, con);

	return con;
}

void consoleDelete(console_t *con)
{
	for (listMoveFirst(con->buffers); !listEOF(con->buffers); listMoveNext(con->buffers)) {
		free(listGetData(con->buffers));
		listRemove(con->buffers);
	}
	listDelete(con->buffers);

	consoleFreeArgs(con);
	
	if (con->prefix)
		free(con->prefix);
	appRemoveCallback(objGetApp(con), consoleCallback, con);

	free(con);
}

void consoleSetOnKeyPress(console_t *console, int (*func)(console_t *, char))
{
	console->onKeyPress = func;
}

int consoleArgc(console_t *console)
{
	return console->argc;
}

char *consoleArgv(console_t *console, int argNum)
{
	if (argNum >= consoleArgc(console) || argNum < 0)
		return NULL;
	return console->argv[argNum];
}
