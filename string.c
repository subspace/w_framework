#include "object.h"
#include "string.h"
#include "util.h"
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#pragma warning(disable:4996)

struct string_s {
	object_t obj;
	char *string;
	unsigned long size;
	unsigned long realSize;
};

string_t *strNew(unsigned long size)
{
	string_t *n;

	n = utilMalloc(sizeof(string_t));
	n->string = utilMalloc(size);
	n->realSize = size;
	n->size = 0;
	
	return n;
}

void strDelete(string_t *str)
{
	free(str->string);
	free(str);
}

void strSet(string_t *str, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	vsprintf(str->string, fmt, args);
	va_end(args);

	str->size = strlen(str->string);
}

char *strGet(string_t *str)
{
	return str->string;
}

void strCopy(string_t *strDest, string_t *strSource)
{
	if (strDest->realSize <= strSource->size) {
		strDest->string = utilRealloc(strDest->string, strSource->size+1);
		strDest->realSize = strSource->size+1;
	}
	strcpy(strDest->string, strSource->string);
	strDest->size = strSource->size;
}

void strCat(string_t *strDest, string_t *strSource)
{
	if ((strDest->size+strSource->size) >= strDest->realSize) {
		char *temp;
		temp = malloc(strDest->size+1);
		strcpy(temp, strDest->string);

		strDest->string = utilRealloc(strDest->string, strDest->size+strSource->size+1);
		strcpy(strDest->string, temp);
		strcpy(strDest->string+strDest->size, strSource->string);
		free(temp);

	} else {
		strcpy(strDest->string+strDest->size, strSource->string);
	}
	strDest->size += strSource->size;
	strDest->realSize = strDest->size+1;
}