#ifndef __PARSER_LOCAL_H__
#define __PARSER_LOCAL_H__

struct parserField_s {
	char	*name;
	int		size;
	char	*delimiter;
};

struct parserLine_s {
	char	*name;
	int		size;
	char	*delimiter;

	list_t	*fields;
};

struct parsedField_s {
	struct parserField_s *field;
	char	*value;
};

struct parsedLine_s {
	struct parserLine_s *line;
	list_t *parsedFields;
};

struct parser_s {
	list_t	*lines;
	list_t	*parsedLines;
	char	*fileName;
};

#endif