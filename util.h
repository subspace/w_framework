#ifndef _UTIL_H_
#define _UTIL_H_

void *utilMalloc(unsigned long size);
void *utilRealloc(void *ptr, unsigned long size);
void utilFree(void **ptr);
char *utilGetFileName(const char *path);

short utilShortSwap(short s);
int utilLongSwap(int l);
float utilFloatSwap(float f);
char *utilGetExePath(void);
char *utilVa (char *fmt, ...);

#if defined(_MSC_VER) && _MSC_VER >= 1400
// vc++ 2005 and up has intrinsics for the BSWAP instruction.
#define ShortSwap _byteswap_ushort
#define LongSwap _byteswap_ulong
#endif

//======================= ENDIAN DECTECTION ==================================
//======================= WIN32 DEFINES ======================================
#ifdef _WIN32
#define __LITTLE_ENDIAN__
#endif

//======================= MAC OS X DEFINES ===================================
// <DyB|Tuna> for mac the gcc defines __BIG_ENDIAN__ and __LITTLE_ENDIAN__ 
//            according to which arch type is selected
//#if defined(MACOS_X)
//#define __LITTLE_ENDIAN__
//#endif

//======================= MAC DEFINES ========================================
// <DyB|Tuna> for mac the gcc defines __BIG_ENDIAN__ and __LITTLE_ENDIAN__ 
//            according to which arch type is selected
//#ifdef __MACOS__
//#define __BIG_ENDIAN__
//#endif

//======================= LINUX DEFINES ======================================
#ifdef __linux__

#if __FLOAT_WORD_ORDER == __BIG_ENDIAN
#define __BIG_ENDIAN__
#elif __FLOAT_WORD_ORDER == __LITTLE_ENDIAN
#define __LITTLE_ENDIAN__
#elif __FLOAT_WORD_ORDER == __PDP_ENDIAN
#define __PDP_ENDIAN__
#endif

#endif

//======================= FreeBSD DEFINES ====================================
#ifdef __FreeBSD__

#include <machine/endian.h>
#if BYTE_ORDER == BIG_ENDIAN
#define __BIG_ENDIAN__
#elif BYTE_ORDER == LITTLE_ENDIAN
#define __LITTLE_ENDIAN__
#elif BYTE_ORDER == PDP_ENDIAN
#define __PDP_ENDIAN__
#endif

#endif

#if defined __BIG_ENDIAN__
	#define utilBigShort(x)		(x)
	#define utilBigLong(x)		(x)
	#define utilBigFloat(x)		(x)
	#define utilLittleShort(x)	utilShortSwap(x)
	#define utilLittleLong(x)	utilLongSwap(x)
	#define utilLittleFloat(x)	utilFloatSwap(x)

#elif defined __LITTLE_ENDIAN__
	#define utilBigShort(x)		utilShortSwap(x)
	#define utilBigLong(x)		utilLongSwap(x)
	#define utilBigFloat(x)		utilFloatSwap(x)
	#define utilLittleShort(x)	(x)
	#define utilLittleLong(x)	(x)
	#define utilLittleFloat(x)	(x)
#endif
#endif