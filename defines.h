#ifndef _DEFINES_H_
#define _DEFINES_H_

#undef false
#undef true

typedef enum {false, true} bool;
typedef unsigned char byte;

typedef unsigned short ushort;
typedef unsigned long ulong;
typedef unsigned int uint;

#endif