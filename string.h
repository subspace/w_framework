#ifndef _STRING_H_
#define _STRING_H_

typedef struct string_s string_t;

string_t *strNew(unsigned long size);
void strDelete(string_t *str);

void strSet(string_t *str, const char *fmt, ...);
char *strGet(string_t *str);
void strCopy(string_t *strDest, string_t *strSource);
void strCat(string_t *strDest, string_t *strSource);

#endif