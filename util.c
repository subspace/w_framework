#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include "defines.h"

char *utilVa (char *fmt, ...)
{
	va_list argptr;
	static char text[1024];
	
	va_start(argptr, fmt);
	vsprintf(text, fmt, argptr);
	va_end(argptr);

	return text;
}

void *utilMalloc(unsigned long size)
{
	void *ptr;

	ptr = malloc(size);
	if (!ptr) {
		printf("failed on allocation of %i bytes.\n", size);
		exit(1);
	}
	return ptr;
}

void *utilRealloc(void *ptr, unsigned long size)
{
	void *ret;

	ret = realloc(ptr, size);
	if (!ret) {
		printf("failed on allocation of %i bytes.\n", size);
		exit(1);
	}
	return ret;
}

short utilShortSwap(short s)
{
	union
	{
		short	s;
		byte	b[2];
	} dat1, dat2;
	dat1.s = s;
	dat2.b[0] = dat1.b[1];
	dat2.b[1] = dat1.b[0];
	return dat2.s;
}

int utilLongSwap(int l)
{
	union
	{
		int		l;
		byte	b[4];
	} dat1, dat2;
	dat1.l = l;
	dat2.b[0] = dat1.b[3];
	dat2.b[1] = dat1.b[2];
	dat2.b[2] = dat1.b[1];
	dat2.b[3] = dat1.b[0];
	return dat2.l;
}

float utilFloatSwap(float f)
{
	union
	{
		float	f;
		byte	b[4];
	} dat1, dat2;
	dat1.f = f;
	dat2.b[0] = dat1.b[3];
	dat2.b[1] = dat1.b[2];
	dat2.b[2] = dat1.b[1];
	dat2.b[3] = dat1.b[0];
	return dat2.f;
}

void utilFree(void **ptr)
{
	if (*ptr) {
		free(*ptr);
		*ptr = 0;
	}
}

char *utilGetExePath(void)
{
	static char ret[MAX_PATH+1];
	int len;

	GetModuleFileNameA(NULL, ret, MAX_PATH+1);
	len = strlen(ret);
	while (len) {
		if (ret[len-1] == '\\') {
			ret[len] = 0;
			break;
		}
		len--;
	}
	return ret;
}

char *utilGetFileName(const char *path)
{
	static char fileName[MAX_PATH+1];
	const char *p;
	
	if (!path)
		return NULL;
	if (strlen(path) < 2)
		return NULL;

	p = path+strlen(path)-2;
	while (*p != '\\' && *p != '/' && p > path)
		p--;
	if (!strlen(p))
		return NULL;

	strcpy(fileName, p);

	return fileName;
}

