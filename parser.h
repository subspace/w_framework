#ifndef __PARSER_H__
#define __PARSER_H__

typedef struct parser_s parser_t;

parser_t *parserNew(void);
void parserDelete(parser_t *parser);

#endif