#include "object.h"
#include "timer.h"
#include "util.h"
#include <string.h>
#include <math.h>
#include <windows.h>

#pragma comment(lib, "winmm.lib")

struct timer_s {
	object_t obj;

	unsigned long frequency;
	unsigned long periodStart;
	void (*timeoutSignal)(timer_t *timer);	
};

unsigned long timerGetTicks(void)
{
	return timeGetTime();
}

int timerCallback(timer_t *timer)
{
	unsigned long now;

	now = timeGetTime();
	if ((now - timer->periodStart) >= timer->frequency) {
		if (timer->timeoutSignal)
			timer->timeoutSignal(timer);
		
		timer->periodStart = now;
	}
	return 0;
}

timer_t *timerNew(app_t *app)
{
	timer_t *timer;

	timer = utilMalloc(sizeof(timer_t));
	memset(timer, 0, sizeof(timer_t));
	objSetApp(timer, app);
	return timer;
}

void timerDelete(timer_t *timer)
{
	appRemoveCallback(objGetApp(timer), timerCallback, timer);
	free(timer);
}

void timerSetFrequency(timer_t *timer, unsigned long frequency)
{
	timer->frequency = abs(frequency);
}

void timerStart(timer_t *timer)
{
	appAddCallback(objGetApp(timer), timerCallback, timer);
	timer->periodStart = timeGetTime();
}

void timerStop(timer_t *timer)
{
	appRemoveCallback(objGetApp(timer), timerCallback, timer);
}

void timerSetOnTimeout(timer_t *timer, void (*signalHandler)(timer_t *timer))
{
	timer->periodStart = timeGetTime();
	timer->timeoutSignal = signalHandler;
}