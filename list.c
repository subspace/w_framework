#include "object.h"
#include "list.h"
#include "util.h"

#include <stdlib.h>
#include <string.h>

enum { LIST_FORWARD, LIST_BACKWARD };

struct listdata_s {
	void *data;
	struct listdata_s *next, *prev;
};

struct list_s {
	object_t obj;

	struct listdata_s *first;
	struct listdata_s *last;
	struct listdata_s *current;

	unsigned char movingDirection;
	int count;
	unsigned char flags;

	void (*onSelectionChange)(struct list_s *);
};

list_t *listNew(void)
{
	list_t *list;

	list = utilMalloc(sizeof(list_t));
	memset(list, 0, sizeof(list_t));
	list->flags = LIST_EOF | LIST_BOF;
	return list;
}

void listDelete(list_t *list)
{
	struct listdata_s *next;

	for (list->current = list->first; list->current; list->current = next)
	{
		next = list->current->next;
		free(list->current);
	}
	free(list);
}

bool listAdd(list_t *list, void *data)
{
	struct listdata_s *ld;

	ld = utilMalloc(sizeof(struct listdata_s));
	memset(ld, 0, sizeof(struct listdata_s));
	ld->data = data;
	ld->prev = list->last;
	if (list->last)
		list->last->next = ld;
	list->last = ld;
	if (!list->first)
		list->first = ld;
	list->count++;

	return true;
}

bool listRemove(list_t *list)
{
	struct listdata_s *newCurrent;

	if (list->current->prev)
		list->current->prev->next = list->current->next;
	if (list->current->next)
		list->current->next->prev = list->current->prev;

	if (list->current == list->first)
		list->first = list->current->next;
	if (list->current == list->last)
		list->last = list->current->prev;

	if (list->movingDirection == LIST_FORWARD)
		newCurrent = list->current->prev;
	if (list->movingDirection == LIST_BACKWARD)
		newCurrent = list->current->next;

	free(list->current);
	list->current = newCurrent;
	list->count--;

	if (list->onSelectionChange && newCurrent)
		list->onSelectionChange(list);

	return true;
}

int listMoveNext(list_t *list)
{
	list->movingDirection = LIST_FORWARD;
	
	if (!list->current) {
		list->flags |= LIST_EOF;
		return LIST_EOF;
	}

	list->current = list->current->next;
	
	if (!list->current) {
		list->flags |= LIST_EOF;
		return LIST_EOF;
	}
	
	list->flags = LIST_OK;

	if (list->onSelectionChange)
		list->onSelectionChange(list);

	return LIST_OK;
}

int listMovePrev(list_t *list)
{
	list->movingDirection = LIST_BACKWARD;

	if (!list->current) {
		list->flags |= LIST_BOF;
		return LIST_BOF;
	}

	list->current = list->current->prev;

	if (!list->current) {
		list->flags |= LIST_BOF;
		return LIST_BOF;
	}

	list->flags = LIST_OK;

	if (list->onSelectionChange)
		list->onSelectionChange(list);

	return LIST_OK;
}

int listMoveFirst(list_t *list)
{
	list->movingDirection = LIST_FORWARD;
	list->current = list->first;
	if (!list->first) {
		list->flags |= LIST_BOF;
		return LIST_BOF;
	}

	list->flags = LIST_OK;

	if (list->onSelectionChange)
		list->onSelectionChange(list);

	return LIST_OK;
}

int listMoveLast(list_t *list)
{
	list->movingDirection = LIST_BACKWARD;
	list->current = list->last;
	if (!list->last) {
		list->flags |= LIST_EOF;
		return LIST_EOF;
	}
	list->flags = LIST_OK;
	
	if (list->onSelectionChange)
		list->onSelectionChange(list);

	return LIST_OK;
}

void *listGetData(list_t *list)
{
	return list->current->data;
}

bool listEOF(list_t *list)
{
	return (list->flags & LIST_EOF) ? true : false;
}

bool listBOF(list_t *list)
{
	return (list->flags & LIST_BOF) ? true : false;
}

void listSetOnSelectionChange(list_t *list, void (*func)(list_t *list))
{
	list->onSelectionChange = func;
}

int listGetCount(list_t *list)
{
	return list->count;
}