#include "object.h"
#include "udp.h"
#include "util.h"
#include "string.h"

#include <stdlib.h>
#include <stdio.h>
#include <windows.h>

struct udp_s {
	object_t obj;

	SOCKET sock;
	char *hostName;
	struct sockaddr_in dest;
	struct sockaddr_in from;
	int flags;

	byte *incomingData;
	unsigned long incomingDataMax;
	unsigned long incomingDataLen;

	void (*onResolveHost)(udp_t *udp);
	void (*onDataArrival)(udp_t *udp, byte *data, unsigned long size);
	void (*onConnect)(udp_t *udp);
};

udp_t *udpNew(app_t *app)
{
	udp_t *obj;
	static bool on = 1;

	if (!appEnableWinsock()) {
		printf("failed to initialize winsock.\n");
		exit(1);
	}

	obj = utilMalloc(sizeof(udp_t));
	memset(obj, 0, sizeof(udp_t));
	objSetApp(obj, app);
	obj->flags = UDP_UNCONNECTED;
	
	obj->sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (obj->sock == SOCKET_ERROR) {
		free(obj);
		return NULL;
	}
	ioctlsocket(obj->sock, FIONBIO, (u_long *)&on);

	obj->incomingData = utilMalloc(8192);
	obj->incomingDataMax = 8192;

	return obj;
}

int udpCallback(udp_t *udp)
{
	int fromLen = sizeof(udp->from);

	udp->incomingDataLen = recvfrom(udp->sock, udp->incomingData, udp->incomingDataMax, 0, (struct sockaddr *)&udp->from, &fromLen);
	if (udp->incomingDataLen == SOCKET_ERROR)
		return 0;

	if (udp->onDataArrival)
		udp->onDataArrival(udp, udp->incomingData, udp->incomingDataLen);
	return 0;
}

void udpDelete(udp_t *udp)
{
	appRemoveCallback(objGetApp(udp), udpCallback, udp);
	free(udp->incomingData);
	closesocket(udp->sock);
	free(udp);
}

void udpSetOnDataArrival(udp_t *udp, void (*func)(udp_t *udp, byte *data, unsigned long size))
{
	udp->onDataArrival = func;
}

void udpSetOnResolveHost(udp_t *udp, void (*func)(udp_t *udp))
{
	udp->onResolveHost = func;
}

unsigned long udpResolveHost(udp_t *udp, const char *host)
{
	unsigned long ret;
		
	ret = inet_addr(host);
	if (ret == INADDR_ANY || ret == INADDR_NONE) {
		struct hostent *he;
		he = gethostbyname(host);
		if (!he)
			return 0;
		ret = *((unsigned long *)he->h_addr_list[0]);
	}

	if (udp->onResolveHost)
		udp->onResolveHost(udp);

	return ret;
}

int udpSetDestination(udp_t *udp, const char *host, unsigned short port)
{
	udp->hostName = (char *)host;

	udp->dest.sin_family = AF_INET;
	udp->dest.sin_port = htons(port);
	udp->dest.sin_addr.s_addr = udpResolveHost(udp, host);
	if (!udp->dest.sin_addr.s_addr)
		return 0;

	if (udp->onConnect)
		udp->onConnect(udp);
	
	appAddCallback(objGetApp(udp), udpCallback, udp);

	return 1;
}

int udpSend(udp_t *udp, byte *data, unsigned long size)
{
	return sendto(udp->sock, data, size, 0, (struct sockaddr *)&udp->dest, sizeof(udp->dest));
}

int udpSendString(udp_t *udp, const char *data)
{
	ulong size;

	size = strlen(data);
	return sendto(udp->sock, data, size, 0, (struct sockaddr *)&udp->dest, sizeof(udp->dest));
}

void udpSetOnConnect(udp_t *udp, void (*func)(udp_t *udp))
{
	udp->onConnect = func;
}

char *udpGetHost(udp_t *udp)
{
	return udp->hostName;
}

ushort udpGetPort(udp_t *udp)
{
	return udp->dest.sin_port;
}