#include "app.h"
#include "timer.h"
#include <stdio.h>
#include <string.h>
#include "udp.h"
#include "console.h"
#include "object.h"
#include "filesystem.h"
#include "pack.h"
#include <conio.h>
#include <stdlib.h>
#include "qwclient.h"

void gotData(udp_t *udp, byte *data, unsigned long size)
{
	printf("[%s]\n", data);
	udpDelete(udp);
}


void executeCommand(console_t *con)
{
	printf("%s\n", consoleArgv(con, 0));
}

int main(int argc, char **argv)
{
	app_t *app;
	console_t *con;
	qwClient_t *qw;
	qwClient_t *qw2;
	qwClient_t *qw3;
	qwClient_t *qw4;
	qwClient_t *qw5;

	app = appNew();

	con = consoleNew(app);
	consoleSetPrefix(con, ">> ");
	consoleSetOnCommand(con, executeCommand);

	qw = qwNew(app);
	qw2 = qwNew(app);
	qw3 = qwNew(app);
	qw4 = qwNew(app);
	qw5 = qwNew(app);
	
	qwConnect(qw, "qw.click21.com.br", 27500);
	//qwConnect(qw, "qw.terra.com.br", 27520);
	//qwConnect(qw2, "qw.terra.com.br", 27521);
	//qwConnect(qw3, "qw.terra.com.br", 27522);
	//qwConnect(qw4, "qw.terra.com.br", 27500);
	//qwConnect(qw5, "qw.terra.com.br", 27530);
	
	//qwDelete(qw);
	return appMainLoop(app);
}