#include "app.h"
#include "list.h"
#include "util.h"
#include <windows.h>
#pragma comment(lib, "ws2_32.lib")

WSADATA wsaData;
bool	winsockStarted;

struct app_s {
	list_t *callbacks;

};

struct callback_s {
	void *object;
	int (*callback)(void *object);
};

app_t *appNew(void)
{
	app_t *app;

	app = utilMalloc(sizeof(app_t));
	app->callbacks = listNew();
	return app;
}

void appDelete(app_t *app)
{
	listDelete(app->callbacks);
	free(app);
}

bool appAddCallback(app_t *app, int (*callback)(void *), void *object)
{
	struct callback_s *cb;

	appRemoveCallback(app, callback, object); //assert there is no replica

	cb = utilMalloc(sizeof(struct callback_s));
	cb->callback = callback;
	cb->object = object;
	
	listAdd(app->callbacks, cb);

	return true;
}

bool appRemoveCallback(app_t *app, int (*callback)(void *), void *object)
{
	struct callback_s *cb;

	for (listMoveFirst(app->callbacks); !listEOF(app->callbacks); listMoveNext(app->callbacks)) {
		cb = listGetData(app->callbacks);
		if (cb->callback == callback && cb->object == object) {
			listRemove(app->callbacks);
			free(cb);
			return true;
		}
	}
	return false;
}

int appRun(app_t *app)
{
	int cbRet;
	struct callback_s *cb;
	
	for (listMoveFirst(app->callbacks); !listEOF(app->callbacks); listMoveNext(app->callbacks)) {
		cb = listGetData(app->callbacks);
		cbRet = cb->callback(cb->object);
		if (cbRet)
			return cbRet;
		
	}
	return 0;
}

int appMainLoop(app_t *app)
{
	int r;
	for (;;) {
		r = appRun(app);
		if (r == -1)
			return r & 0x8f;
		Sleep(1);
	}
	return 0;
}

bool appEnableWinsock(void)
{
	if (winsockStarted)
		return true;

	if (!WSAStartup(MAKEWORD(2,2), &wsaData)) {
		winsockStarted = true;
		return true;
	}
	return false;
}

bool appDisableWinsock(void)
{
	if (winsockStarted) {
		WSACleanup();
		return true;
	}
	return true;
}