#ifndef _TIMER_H_
#define _TIMER_H_

typedef struct timer_s timer_t;

timer_t *timerNew(app_t *app);
void timerDelete(timer_t *timer);

void timerSetFrequency(timer_t *timer, unsigned long frequency);
void timerStart(timer_t *timer);
void timerStop(timer_t *timer);
void timerSetOnTimeout(timer_t *timer, void (*signalHandler)(timer_t *timer));
unsigned long timerGetTicks(void);

#endif