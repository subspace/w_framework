#ifndef _OBJECT_H_
#define _OBJECT_H_

#include "app.h"

struct object_s {
	app_t	*app;
	void	*clientData;
};

typedef struct object_s object_t;

void objSetApp(void *object, app_t *app);
app_t *objGetApp(void *object);
void objSetClientData(void *object, void *clientData);
void *objGetClientData(void *object);

#endif